[![veri.data logo](https://media-exp1.licdn.com/dms/image/C4E0BAQEcDaRZZJhfNg/company-logo_200_200/0/1639454535251?e=2159024400&v=beta&t=EwbDBRZ-kATJoIqDdHGNr7Rbul0aYU9ydoGrBTmrYG8)](https://www.linkedin.com/company/veri-data/)

[![GitLab latest release](https://badgen.net/gitlab/release/veri-data-open-source/veri.data/)](https://gitlab.com/veri-data-open-source/veri.data/-/releases) [![GitLab last commit](https://badgen.net/gitlab/last-commit/veri-data-open-source/veri.data/)](https://gitlab.com/veri-data-open-source/veri.data//-/commits) [![GitLab issues open](https://badgen.net/gitlab/open-issues/veri-data-open-source/veri.data)](https://gitlab.com/veri-data-open-source/veri.data/-/boards)

[[_TOC_]]

# About veri.data

This is the veri.data open source project. In this README you will find background information about the project (the why, what & how) as well as the current status and the outlook of the project. 
veri.data´s goal is to provide out-of-the-box Excel-based data modeling & data management approach that scales to most complex industrial scenarios, as well as ready-to-use example models within the health-care and finance sector.

Our current [excel version](https://gitlab.com/veri-data-open-source/veri.data/-/blob/main/veri.data_opensource_metadata_generator.xlsm) can be found in the repository above.

# Vision
Our vision is to empower startups to use a scaled-data modeling and management approach without having to build up a full scale data architecture. We also want to enable international coorperations and their large programs by doing control, conceptual and physical data modeling in a lean and Excel based approach. 

In case you want to learn more about our approach please have a look at our [wiki](https://gitlab.com/veri-data-open-source/veri.data/-/wikis/Navigation-Page)

# Mission 
Our mission is to evolve and document the approach with an agile team consisting of talented students from Kinshasa and the university of Mannheim. The leader of the Software Technology chair Prof. Dr. Colin Atkinson whom supports the activities as well.

# Prerequisites and Installation

For using the Excel you can download either the [Excel](https://gitlab.com/veri-data-open-source/veri.data/-/blob/main/veri.data_opensource_metadata_generator.xlsm) from this repository or a zipped file in [releases](https://gitlab.com/veri-data-open-source/veri.data/-/releases) depending on your needs. As requirement to use the Excel you will need MS Office to use it. 
For using mcore it is important to download the tool from the former website of Veri.Data to use the here published files about Mcore. By following the [link](http://www.montages.com/montages-thesis.html), you are able to get the mcore tool.
A detailed guide for the mcore installation can be found in our [wiki](https://gitlab.com/veri-data-open-source/veri.data/-/wikis/4.-Mcore/4.2-How-to-work-with-Mcore/4.2.1-Installation).

# Getting Started
The first step to work with this repository is to read the [wiki](https://gitlab.com/veri-data-open-source/veri.data/-/wikis/home). The wiki is a living document and continuously changing, which means that is always under the state of in progress.

# License
You can find our License for our planned Java-based solution [here](https://gitlab.com/veri-data-open-source/veri.data/-/blob/dc87379cd304768ac777e84b3f4b7c889d1c44e2/LICENSE).

# Future Work

- Extension of approach to be able to show associations between the database objects
- Complete prototype and document a usecase of using pre-defined data models for startups


# Contact and Contribution

In case you have any issue, questions or want to provide feedback contact us via our E-Mail address: [veriopensource@gmail.com](mailto:veriopensource@gmail.com).

[Here you can find the current team, its alumni and can apply for contributing to the project.](https://gitlab.com/groups/veri-data-open-source/-/group_members)

# Contributors

Jan-Nicklas Adler ([LinkedIn](https://promatur.com?mtm_campaign=referral&mtm_source=GitLab&mtm_medium=Veri.Data%20Project&mtm_content=Jan-Nicklas%20Adler&mtm_group=university&mtm_placement=contributors#about)) · Keyvan Amiri Elyasi ([LinkedIn](https://www.linkedin.com/in/keyvan-amiri-elyasi-563b7439/)) · Niklas Hoedt ([LinkedIn](https://www.linkedin.com/in/niklashoedt/)) · Rahul S Joshi ([LinkedIn](https://www.linkedin.com/in/rahul-s-joshi-2156179b)) · Rouxin Ren ([LinkedIn](https://www.linkedin.com/in/ruren/)) · Peter Schwind ([LinkedIn](https://www.linkedin.com/in/peter-schwind-887858218)) · Amalia Shaliha ([LinkedIn](https://www.linkedin.com/in/amalia-shaliha-736592184/)) 

# Veri.Data 2.0

The Veri.Data 2.0: The focus of this evolutionary project of Veri.Data is to classify existing and onboard new Business Records.​

What does Onboarding mean?: It is the process of adding new data to the existing Veri.Data Model. The proper domain and data model components are created or modified as part of a process that guides data models.​

For more detailed information about the Onboarding Model, please check [here](https://gitlab.com/veri-data-open-source/veri.data/-/wikis/veri.data/0.-Introduction/0.3-Business-Record-Onboarding-and-Classification).

# New Features and Achievements 

With the new projects goals achived, the new Veri.Data Excel file has a New Business Record Onboarding Module which can be looked into in this link ([Onboarding Modules](https://gitlab.com/veri-data-open-source/veri.data/-/wikis/veri.data/2.-Excel/2.0-New-Onboarding-Modules))

For a deep-dive view of the achievements and experiments done with respect to this new evolutionary project, please view this presentation.

# Contributors

Thomas Schmidt ([LinkedIn](https://www.linkedin.com/in/thomasgschmidt/)) · Anastasiia Belkina ([LinkedIn](https://www.linkedin.com/in/anastasiia-belkina-3a6878206/)) · Julius Döllinger ([LinkedIn](https://www.linkedin.com/in/julius-döllinger-45945617a/)) · Ashish Mandayam ([LinkedIn](https://www.linkedin.com/in/ashish-u-mandayam/)) · Franziska Dreier ([LinkedIn](https://www.linkedin.com/in/franziska-dreier-088956222/))
